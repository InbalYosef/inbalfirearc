// controllers/ArticleController.js

const articleService = require('../BL/ArticleService');

// Get all articles
function getAllArticles(req, res) {
  articleService.getAllArticles((error, articles) => {
    if (error) {
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    res.json(articles); // Return all articles
  });
}

// Get an article by id
function getArticleById(req, res) {
  const { id } = req.params;
  articleService.getArticleById(id, (error, article) => {
    if (error) {
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    if (!article) {
      res.status(404).json({ message: 'Article not found' });
      return;
    }
    res.json(article); // Return the single article
  });
}

// Update an article by id
function updateArticleById(req, res) {
  const { id } = req.params;
  const updatedArticle = req.body;

  articleService.updateArticleById(id, updatedArticle, (error, updatedArticle) => {
    if (error) {
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    if (!updatedArticle) {
      res.status(404).json({ message: 'Article not found' });
      return;
    }
    res.json(updatedArticle); // Return the updated article
  });
}

// Delete an article by id
function deleteArticleById(req, res) {
  const { id } = req.params;
  articleService.deleteArticleById(id, (error, success) => {
    if (error) {
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    if (!success) {
      res.status(404).json({ message: 'Article not found' });
      return;
    }
    res.json({ message: 'Article deleted successfully' });
  });
}

module.exports = {
  getAllArticles,
  getArticleById,
  updateArticleById,
  deleteArticleById,
};
