// routes/api.js

const express = require('express');
const articleController = require('../controllers/ArticleController');

const router = express.Router();

// Get all articles
router.get('/articles', articleController.getAllArticles);

// Get an article by ID
router.get('/articles/:id', articleController.getArticleById);

// Update an article by ID
router.put('/articles/:id', articleController.updateArticleById);

// Delete an article by ID
router.delete('/articles/:id', articleController.deleteArticleById);

module.exports = router;
