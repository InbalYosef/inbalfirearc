//ArticleDAL.js
const db = require('../DB/db');

class Article {
  static getAll(callback) {
    db.query('SELECT * FROM articles', (err, results) => {
      if (err) throw err;
      callback(results);
    });
  }

  static getById(id, callback) {
    db.query('SELECT * FROM articles WHERE id = ?', [id], (err, results) => {
      if (err) throw err;
      if (results.length === 0) {
        callback(null); // Article not found
      } else {
        callback(results[0]);
      }
    });
  }

  static updateById(id, updatedArticle, callback) {
    db.query('UPDATE articles SET ? WHERE id = ?', [updatedArticle, id], (err, results) => {
      if (err) throw err;
      callback(results.affectedRows > 0); // Returns true if an article was updated
    });
  }

  static deleteById(id, callback) {
    db.query('DELETE FROM articles WHERE id = ?', [id], (err, results) => {
      if (err) throw err;
      callback(results.affectedRows > 0); // Returns true if an article was deleted
    });
  }
}

module.exports = Article;
