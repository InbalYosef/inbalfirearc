// models/articleModel.js

class Article {
  constructor(id, title, description, body) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.body = body;
  }
}

module.exports = Article;