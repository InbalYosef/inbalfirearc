// ArticleService.js - BL
const ArticleDAL = require('../DAL/ArticleDAL');
const Article = require('../Models/ArticleModel');

// Get all articles
function getAllArticles(callback) {
  ArticleDAL.getAll((error, results) => {
    if (error) {
      callback(error, null);
      return;
    }
    // Convert database results to Article objects
    const articles = results.map((row) => new Article(row.id, row.title, row.description, row.body));
    callback(null, articles);
  });
}

// Get an article by id
function getArticleById(id, callback) {
  ArticleDAL.getById(id, (error, result) => {
    if (error) {
      callback(error, null);
      return;
    }
    if (!result) {
      callback({ message: 'Article not found' }, null);
      return;
    }
    // Convert the database result to an Article object
    const article = new Article(result.id, result.title, result.description, result.body);
    callback(null, article);
  });
}

// Update an article by id
function updateArticleById(id, updatedArticle, callback) {
  ArticleDAL.updateById(id, updatedArticle, (error, updated) => {
    if (error) {
      callback(error, null);
      return;
    }
    if (!updated) {
      callback({ message: 'Article not found' }, null);
      return;
    }
    
    // Fetch the updated article by its ID
    getArticleById(id, (fetchError, article) => {
      if (fetchError) {
        callback(fetchError, null);
        return;
      }
      callback(null, article);
    });
  });
}

// Delete an article by id
function deleteArticleById(id, callback) {
  ArticleDAL.deleteById(id, (error, deleted) => {
    if (error) {
      callback(error, null);
      return;
    }
    if (!deleted) {
      callback({ message: 'Article not found' }, false);
      return;
    }
    callback(null, true);
  });
}

module.exports = {
  getAllArticles,
  getArticleById,
  updateArticleById,
  deleteArticleById,
};
